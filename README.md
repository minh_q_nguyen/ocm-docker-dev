# How to setup local OCM project with docker
0. clone `mem-system` project from its repository
1. clone this repository inside `mem-system/src` dir
2. copy the 2 files: `_docker-compose.yml` and `_Dockerfile` under this directory to `mem-system/src`
3. go to file: `mem-system/src/mem-domain/src/main/resources/env-onp/gears-onpremise_definition.properties`, update these keys:

```
HOST=mongodb-dev:27017
redis.host.primary=redis-db:6379
```
these are the names of the docker-compose service and container_port (not host port)

(Optional, only for the first time setup wizard) `mem-system/src/mem-console/src/main/java/com/opswat/mem/console/activation/License.java`, for linux machine, the md5sum was outdated and must be re-calculate and updated:
first, calculate the md5sum of the license:
```bash
$ md5sum mem-console/target/classes/linux-x86-64/libLicenseUtils.so
```
then add to the LINUX_SUM_LIST of the `License.java`l file

Also fix all the NPE on the file `mem-console/src/main/java/com/opswat/mem/console/services/impl/OnpInitDataServiceImpl.java`, function `updateOnpConfig()`

4. build all modules:

```bash
$ cd mem-system/src
$ mvn clean install
```

5. copy all dependencies a web app (e.g., console, scheduler) need(*):
this step require the maven plugin: `maven-dependency-plugin`

```bash
# to a web module, console for example
$ cd mem-console
$ mvn dependency:copy-dependencies -DoutputDirectory=../ocm-docker-dev/dev-libs
```

6.run docker-compose:

```bash
$ docker-compose up --build
```

7. insert config data from all files: mem-system/src/mem-oc-scripts/init-data/ to mongodb

8. navigate to http://localhost:8080/console and start the setup wizard. Done!

(*): in development mode, we should deploy our app to tomcat by compiled code, not an archived package, the command only copy dependencies if they're not exist in the output dir and are required by the web application. This will speed up the deployment much faster.
